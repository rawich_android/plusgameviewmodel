package buu.rawich.plusgamev2.title

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import buu.rawich.plusgamev2.R
import buu.rawich.plusgamev2.databinding.FragmentTitleBinding



// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


/**
 * A simple [Fragment] subclass.
 * Use the [TitleFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class TitleFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private lateinit var binding: FragmentTitleBinding
    private lateinit var titleviewModel: TitleViewModel
    private  var menu =0

    override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,
            R.layout.fragment_title, container, false)

        titleviewModel = TitleViewModel()

        titleviewModel.eventNextToPlusMode.observe(viewLifecycleOwner, Observer { eventNextToPlusMode ->
            if(eventNextToPlusMode){
                menu = 1
                view?.findNavController()?.navigate(TitleFragmentDirections.actionTitleFragmentToPlayGameFragment(0,0,0,menu))
                titleviewModel.onNextToPlusModeComplete()
            }
        })

        titleviewModel.eventNextToMinusMode.observe(viewLifecycleOwner, Observer { eventNextToMinusMode ->
            if(eventNextToMinusMode){
                menu = 2
                view?.findNavController()?.navigate(TitleFragmentDirections.actionTitleFragmentToPlayGameFragment(0,0,0,menu))
                titleviewModel.onNextToMinusModeComplete()
            }
        })
        titleviewModel.eventNextToMultipliedMode.observe(viewLifecycleOwner, Observer { eventNextToMultipliedMode ->
            if(eventNextToMultipliedMode){
                menu = 3
                view?.findNavController()?.navigate(TitleFragmentDirections.actionTitleFragmentToPlayGameFragment(0,0,0,menu))
                titleviewModel.onNextToMultipliedModeComplete()
            }
        })

        titleviewModel.eventNextToDivideMode.observe(viewLifecycleOwner, Observer { eventNextToDivideMode ->
            if(eventNextToDivideMode){
                menu = 4
                view?.findNavController()?.navigate(TitleFragmentDirections.actionTitleFragmentToPlayGameFragment(0,0,0,menu))
                titleviewModel.onNextToDivideModeComplete()
            }
        })

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            requireActivity().finish()
        }

        binding.titleViewModel = titleviewModel
        return binding.root
    }

}