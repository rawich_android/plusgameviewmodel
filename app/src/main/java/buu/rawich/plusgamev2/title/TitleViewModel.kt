package buu.rawich.plusgamev2.title

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel


class TitleViewModel () : ViewModel() {


    private val _eventNextToPlusMode = MutableLiveData<Boolean>()
    val eventNextToPlusMode: LiveData<Boolean>
        get() = _eventNextToPlusMode

    fun onNextToPlusMode () {
        _eventNextToPlusMode.value = true
    }

    fun onNextToPlusModeComplete () {
        _eventNextToPlusMode.value = false
    }

    private val _eventNextToMinusMode = MutableLiveData<Boolean>()
    val eventNextToMinusMode: LiveData<Boolean>
        get() = _eventNextToMinusMode

    fun onNextToMinusMode () {
        _eventNextToMinusMode.value = true
    }

    fun onNextToMinusModeComplete () {
        _eventNextToMinusMode.value = false
    }

    private val _eventNextToMultipliedMode = MutableLiveData<Boolean>()
    val eventNextToMultipliedMode: LiveData<Boolean>
        get() = _eventNextToMultipliedMode

    fun onNextToMultipliedMode () {
        _eventNextToMultipliedMode.value = true
    }

    fun onNextToMultipliedModeComplete () {
        _eventNextToMultipliedMode.value = false
    }

    private val _eventNextToDivideMode = MutableLiveData<Boolean>()
    val eventNextToDivideMode: LiveData<Boolean>
        get() = _eventNextToDivideMode

    fun onNextToDivideMode () {
        _eventNextToDivideMode.value = true
    }

    fun onNextToDivideModeComplete () {
        _eventNextToDivideMode.value = false
    }


}