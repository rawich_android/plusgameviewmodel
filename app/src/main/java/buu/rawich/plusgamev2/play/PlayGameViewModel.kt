package buu.rawich.plusgamev2.play

import android.os.CountDownTimer
import android.text.format.DateUtils
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import buu.rawich.plusgamev2.Score
import kotlin.random.Random

class PlayGameViewModel(score: Score, menu: Int, finalScore: Int ) : ViewModel() {
    companion object {

        // Time when the game is over
        private const val DONE = 0L

        // Countdown time interval
        private const val ONE_SECOND = 1000L

        // Total time for the game
        private const val COUNTDOWN_TIME = 30000L

    }
    // Countdown time
    private val _currentTime = MutableLiveData<Long>()
    private val currentTime: LiveData<Long>
        get() = _currentTime

    private val timer: CountDownTimer

    val currentTimeString = Transformations.map(currentTime) { time ->
        DateUtils.formatElapsedTime(time)
    }

    private val _txtAns = MutableLiveData<String>()
    val txtAns: LiveData<String>
        get() = _txtAns

    private val _finalScore = MutableLiveData<Int>()
    val finalScore: LiveData<Int>
        get() = _finalScore

    private val _menu = MutableLiveData<Int>()
    val menu: LiveData<Int>
        get() = _menu

//    private val _question = MutableLiveData<Question>()
//    val question: LiveData<Question>
//        get() = _question

    private val _answer = MutableLiveData<Int>()
    val answer: LiveData<Int>
        get() = _answer

    private val _num1 = MutableLiveData<Int>()
    val num1: LiveData<Int>
        get() = _num1

    private val _num2 = MutableLiveData<Int>()
    val num2: LiveData<Int>
        get() = _num2

    private val _txtSign = MutableLiveData<String>()
    val txtSign: LiveData<String>
        get() = _txtSign

    private val _btn1 = MutableLiveData<Int>()
    val btn1: LiveData<Int>
        get() = _btn1

    private val _btn2 = MutableLiveData<Int>()
    val btn2: LiveData<Int>
        get() = _btn2

    private val _btn3 = MutableLiveData<Int>()
    val btn3: LiveData<Int>
        get() = _btn3

    private val _eventNext = MutableLiveData<Boolean>()
    val eventNext: LiveData<Boolean>
        get() = _eventNext

    private val _eventEndGame = MutableLiveData<Boolean>()
    val eventEndGame: LiveData<Boolean>
        get() = _eventEndGame

    private val _correct = MutableLiveData<Int>()
    val correct: LiveData<Int>
        get() = _correct

    private val _incorrect = MutableLiveData<Int>()
    val incorrect: LiveData<Int>
        get() = _incorrect

    private val _result = MutableLiveData<Int>()
    val result: LiveData<Int>
        get() = _result

    private val _eventScoreChange = MutableLiveData<Boolean>()
    val eventScoreChange : LiveData<Boolean>
        get() = _eventScoreChange

    private val _score = MutableLiveData<Score>()
    val score: LiveData<Score>
        get() = _score

    private val _resultBoolean = MutableLiveData<Boolean>()
    val resultBoolean : LiveData<Boolean>
        get() = _resultBoolean

    private val _hasClicked = MutableLiveData<Boolean>()
    val hasClicked : LiveData<Boolean>
        get() = _hasClicked

    init {
        _finalScore.value = 0
        _correct.value = 0
        _incorrect.value = 0
        _menu.value = menu
        _score.value = score
        _num1.value = 0
        _num2.value = 0
        _txtSign.value = ""
        _result.value = 0
        _txtAns.value = ""
        _resultBoolean.value = false
        //_question.value = Question(menu)
        play(_menu.value!!)

//         Creates a timer which triggers the end of the game when it finishes
        timer = object : CountDownTimer(COUNTDOWN_TIME, ONE_SECOND) {

            override fun onTick(millisUntilFinished: Long) {
                _currentTime.value = millisUntilFinished / ONE_SECOND
                _txtAns.value =  "..."
            }

            override fun onFinish() {
                _currentTime.value = DONE
                onEndGame()
            }
        }

        timer.start()

    }
    private fun play(menu: Int) {
        _result.value = setQuestion(menu)
        randomButton(_result.value!!)

    }
    fun onSkip() {
        play(_menu.value!!)
    }
    fun checkResult(finalScore: Int) {
        if (answer.value == result.value) {
            _finalScore.value = finalScore + 1
            ansCorrect()
        } else {
            _finalScore.value = finalScore - 1

            ansIncorrect()
        }
        _eventScoreChange.value = true
        play(_menu.value!!)
    }


    private fun ansIncorrect() {
        _txtAns.value = ""+ _answer.value + " is correct"
        _score.value?.onInCorrect()
        _resultBoolean.value = false
    }

    private fun ansCorrect() {
        _txtAns.value =  "Correct"
        _score.value?.onCorrect()
        _resultBoolean.value = true

    }

    private fun randomButton(result: Int) {
        val randomNum = Random.nextInt(1, 4)
        if (randomNum == 1) {
            _btn1.value = result
            _btn2.value = result?.plus(1)
            _btn3.value = result?.minus(1)
        } else if (randomNum == 2) {
            _btn1.value = result?.plus(1)
            _btn2.value = result
            _btn3.value = result?.minus(1)
        } else {
            _btn1.value = result?.plus(1)
            _btn2.value = result?.minus(1)
            _btn3.value = result

        }
    }


    private fun setQuestion(menu: Int): Int {
        var result = 0
        var number1 = Random.nextInt(0, 10)
        var number2 = Random.nextInt(0, 10)
        _num1.value = number1
        _num2.value = number2

        if (menu == 1){
            result = number1 + number2
            _txtSign.value = "+"
        }else if(menu == 2){
            result = number1 - number2
            _txtSign.value = "-"
        }else if (menu == 3) {
            result = number1 * number2
            _txtSign.value = "*"
        }else {
            while (true){
                number2 = Random.nextInt(1,10)
                if (number1 % number2 ==0){
                    result = number1 / number2
                    _txtSign.value = "/"
                    _num2.value = number2
                    break
                }
            }
        }
        return result
    }

    fun onHasClicked(answer: Int) {
        Log.i("result", _hasClicked.value.toString())
        _answer.value = answer
        _hasClicked.value = true

    }

    fun onHasClickedFinished() {
        _hasClicked.value = false
    }



    /** Method for the game completed event **/

    fun onEndGame() {
        _eventEndGame.value = true
    }

    fun onNextComplete() {
        _eventNext.value = false
    }

    override fun onCleared() {
        super.onCleared()
        timer.cancel()
    }
}


