package buu.rawich.plusgamev2.play

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import buu.rawich.plusgamev2.Score


class PlayGameViewModelFactory (private val score : Score = Score(), private val menu : Int, private val finalScore : Int): ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PlayGameViewModel::class.java)) {
            return PlayGameViewModel(score, menu,finalScore) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}